<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\Tests\FileSource;

use Hermes\Asset\FileSource\ContentTypeFileSourceDecorator;
use Hermes\Asset\FileSource\FileSource;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * Class ContentTypeFileSourceDecoratorTest.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ContentTypeFileSourceDecoratorTest extends TestCase
{
    public function testThatContentTypeIsOk(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $uri = $this->createMock(UriInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $fileSource = $this->createMock(FileSource::class);

        $uri->expects($this->exactly(2))
            ->method('getPath')
            ->willReturn('/some/good/path.json');
        $request->expects($this->exactly(2))
            ->method('getUri')
            ->willReturn($uri);

        $fileSource->expects($this->once())
            ->method('serveFile')
            ->with($request)
            ->willReturn($response);
        $fileSource->expects($this->once())
            ->method('shouldServeFile')
            ->with($request)
            ->willReturn(true);

        $response->expects($this->once())
            ->method('withHeader')
            ->willReturn($response);

        $concreteFileSource = new ContentTypeFileSourceDecorator($fileSource);

        $this->assertTrue($concreteFileSource->shouldServeFile($request));
        $this->assertInstanceOf(ResponseInterface::class, $concreteFileSource->serveFile($request));
    }

    public function testThatInvalidContentTypeDoesNotPass(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $uri = $this->createMock(UriInterface::class);
        $fileSource = $this->createMock(FileSource::class);

        $uri->expects($this->once())
            ->method('getPath')
            ->willReturn('/some/invalid/path.php');
        $request->expects($this->once())
            ->method('getUri')
            ->willReturn($uri);

        $fileSource->expects($this->never())
            ->method('serveFile');
        $fileSource->expects($this->once())
            ->method('shouldServeFile')
            ->with($request)
            ->willReturn(true);

        $concreteFileSource = new ContentTypeFileSourceDecorator($fileSource);

        $this->assertFalse($concreteFileSource->shouldServeFile($request));
    }
}
