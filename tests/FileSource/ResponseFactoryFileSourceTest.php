<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\Tests\FileSource;

use Hermes\Asset\FileSource\ResponseFactoryFileSource;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class ResponseFactoryFileSourceTest.
 */
class ResponseFactoryFileSourceTest extends TestCase
{
    public function testThatResponseIsCreated(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $responseFactory = $this->createMock(ResponseFactoryInterface::class);

        $request->expects($this->once())
            ->method('getMethod')
            ->willReturn('GET');

        $responseFactory->expects($this->once())
            ->method('createResponse')
            ->willReturn($response);

        $fileSource = new ResponseFactoryFileSource($responseFactory);

        $this->assertTrue($fileSource->shouldServeFile($request));
        $this->assertInstanceOf(ResponseInterface::class, $fileSource->serveFile($request));
    }

    public function testThatResponseIsNotCreated(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $responseFactory = $this->createMock(ResponseFactoryInterface::class);

        $request->expects($this->once())
            ->method('getMethod')
            ->willReturn('POST');

        $responseFactory->expects($this->never())
            ->method('createResponse');

        $fileSource = new ResponseFactoryFileSource($responseFactory);

        $this->assertFalse($fileSource->shouldServeFile($request));
    }
}
