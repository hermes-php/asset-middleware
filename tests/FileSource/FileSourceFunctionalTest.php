<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\Tests\FileSource;

use Hermes\Asset\FileSource\AttachBodyFileSourceDecorator;
use Hermes\Asset\FileSource\ContentTypeFileSourceDecorator;
use Hermes\Asset\FileSource\ResponseFactoryFileSource;
use PHPUnit\Framework\TestCase;
use Zend\Diactoros\ResponseFactory;
use Zend\Diactoros\ServerRequest;
use Zend\Diactoros\StreamFactory;

/**
 * Class FileSourceFunctionalTest.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class FileSourceFunctionalTest extends TestCase
{
    public function testSuccessfulResponseWithZendDiactoros(): void
    {
        $responseFactory = new ResponseFactory();
        $streamFactory = new StreamFactory();

        $baseFileSource = new ResponseFactoryFileSource($responseFactory);
        $bodyFileSource = new AttachBodyFileSourceDecorator($baseFileSource, $streamFactory, __DIR__);
        $contentTypeFileSource = new ContentTypeFileSourceDecorator($bodyFileSource);

        $request = new ServerRequest([], [], '/file.json', 'GET', tmpfile());

        $this->assertTrue($contentTypeFileSource->shouldServeFile($request));

        $response = $contentTypeFileSource->serveFile($request);

        $this->assertStringEqualsFile(__DIR__.'/file.json', $response->getBody()->getContents());
        $this->assertSame('application/json;charset=utf8', $response->getHeader('Content-Type')[0]);
    }
}
