<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\Tests\FileSource;

use Hermes\Asset\FileSource\AttachBodyFileSourceDecorator;
use Hermes\Asset\FileSource\FileSource;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

/**
 * Class AttachBodyFileSourceDecoratorTest.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class AttachBodyFileSourceDecoratorTest extends TestCase
{
    public function testThatBodyIsAttached(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $uri = $this->createMock(UriInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $streamFactory = $this->createMock(StreamFactoryInterface::class);
        $stream = $this->createMock(StreamInterface::class);
        $fileSource = $this->createMock(FileSource::class);

        $filename = __DIR__.'/file.json';

        $uri->expects($this->exactly(3))
            ->method('getPath')
            ->willReturn('/file.json');
        $request->expects($this->exactly(3))
            ->method('getUri')
            ->willReturn($uri);

        $fileSource->expects($this->once())
            ->method('serveFile')
            ->with($request)
            ->willReturn($response);
        $fileSource->expects($this->once())
            ->method('shouldServeFile')
            ->with($request)
            ->willReturn(true);

        $streamFactory->expects($this->once())
            ->method('createStreamFromFile')
            ->with($filename)
            ->willReturn($stream);

        $response->expects($this->once())
            ->method('withBody')
            ->willReturn($response);
        $response->expects($this->once())
            ->method('withHeader')
            ->with('Content-Length')
            ->willReturn($response);

        $concreteFileSource = new AttachBodyFileSourceDecorator($fileSource, $streamFactory, __DIR__);

        $this->assertTrue($concreteFileSource->shouldServeFile($request));
        $this->assertInstanceOf(ResponseInterface::class, $concreteFileSource->serveFile($request));
    }

    public function testThatWrongPathGivesFalse(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $uri = $this->createMock(UriInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $streamFactory = $this->createMock(StreamFactoryInterface::class);
        $fileSource = $this->createMock(FileSource::class);

        $uri->expects($this->once())
            ->method('getPath')
            ->willReturn('/wrong-file.json');
        $request->expects($this->once())
            ->method('getUri')
            ->willReturn($uri);

        $fileSource->expects($this->never())
            ->method('serveFile');
        $fileSource->expects($this->once())
            ->method('shouldServeFile')
            ->with($request)
            ->willReturn(true);

        $streamFactory->expects($this->never())
            ->method('createStreamFromFile');

        $response->expects($this->never())
            ->method('withBody');

        $concreteFileSource = new AttachBodyFileSourceDecorator($fileSource, $streamFactory, __DIR__);

        $this->assertFalse($concreteFileSource->shouldServeFile($request));
    }
}
