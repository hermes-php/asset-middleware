<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\Tests\Factory;

use Hermes\Asset\AssetMiddleware;
use Hermes\Asset\Exception\FactoryException;
use Hermes\Asset\Factory\AssetMiddlewareFactory;
use Hermes\Asset\FileSource\FileSource;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/**
 * Class AssetMiddlewareFactoryTest.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class AssetMiddlewareFactoryTest extends TestCase
{
    public function testFactoryWorks(): void
    {
        $container = $this->createMock(ContainerInterface::class);
        $fileSource = $this->createMock(FileSource::class);

        $container->expects($this->once())
            ->method('has')
            ->with(FileSource::class)
            ->willReturn(true);
        $container->expects($this->once())
            ->method('get')
            ->with(FileSource::class)
            ->willReturn($fileSource);

        $factory = new AssetMiddlewareFactory();

        $instance = $factory($container);
        $this->assertInstanceOf(AssetMiddleware::class, $instance);
    }

    public function testExceptionWhenServiceDoesNotExist(): void
    {
        $container = $this->createMock(ContainerInterface::class);

        $container->expects($this->once())
            ->method('has')
            ->with(FileSource::class)
            ->willReturn(false);
        $container->expects($this->never())
            ->method('get');

        $factory = new AssetMiddlewareFactory();

        $this->expectException(FactoryException::class);
        $instance = $factory($container);
    }
}
