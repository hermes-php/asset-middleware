<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\Tests;

use Hermes\Asset\AssetMiddleware;
use Hermes\Asset\FileSource\FileSource;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AssetMiddlewareTest extends TestCase
{
    public function testThatAssetIsProcessed(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $httpHandler = $this->createMock(RequestHandlerInterface::class);
        $fileSource = $this->createMock(FileSource::class);

        $fileSource->expects($this->once())
            ->method('shouldServeFile')
            ->with($request)
            ->willReturn(true);
        $fileSource->expects($this->once())
            ->method('serveFile')
            ->with($request)
            ->willReturn($response);

        $middleware = new AssetMiddleware($fileSource);

        $response = $middleware->process($request, $httpHandler);
        $this->assertInstanceOf(ResponseInterface::class, $response);
    }

    public function testNextHandlerIsCalledOnNotProcessableRequest(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $httpHandler = $this->createMock(RequestHandlerInterface::class);
        $fileSource = $this->createMock(FileSource::class);

        $httpHandler->expects($this->once())
            ->method('handle')
            ->with($request)
            ->willReturn($response);

        $fileSource->expects($this->once())
            ->method('shouldServeFile')
            ->with($request)
            ->willReturn(false);
        $fileSource->expects($this->never())
            ->method('serveFile');

        $middleware = new AssetMiddleware($fileSource);

        $response = $middleware->process($request, $httpHandler);
        $this->assertInstanceOf(ResponseInterface::class, $response);
    }
}
