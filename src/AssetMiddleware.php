<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset;

use Hermes\Asset\FileSource\FileSource;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class AssetMiddleware.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class AssetMiddleware implements MiddlewareInterface
{
    /**
     * @var FileSource
     */
    private $fileSource;

    /**
     * AssetMiddleware constructor.
     *
     * @param FileSource $fileSource
     */
    public function __construct(FileSource $fileSource)
    {
        $this->fileSource = $fileSource;
    }

    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->fileSource->shouldServeFile($request)) {
            return $this->fileSource->serveFile($request);
        }

        return $handler->handle($request);
    }
}
