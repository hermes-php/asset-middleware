<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\Exception;

use Hermes\Asset\AssetMiddleware;

/**
 * Class FactoryException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class FactoryException extends \RuntimeException
{
    /**
     * @param string $service
     *
     * @return FactoryException
     */
    public static function fromRequiredService(string $service): FactoryException
    {
        return new self(sprintf('Cannot instantiate %s: required service %s is not in the container', AssetMiddleware::class, $service));
    }
}
