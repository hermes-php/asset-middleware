<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\FileSource;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;

/**
 * Class AttachBodyFileSourceDecorator.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class AttachBodyFileSourceDecorator implements FileSource
{
    /**
     * @var FileSource
     */
    private $fileSource;
    /**
     * @var StreamFactoryInterface
     */
    private $streamFactory;
    /**
     * @var string
     */
    private $staticFilesDir;

    /**
     * AttachBodyFileSourceDecorator constructor.
     *
     * @param FileSource             $fileSource
     * @param StreamFactoryInterface $streamFactory
     * @param string                 $staticFilesDir
     */
    public function __construct(FileSource $fileSource, StreamFactoryInterface $streamFactory, string $staticFilesDir)
    {
        $this->fileSource = $fileSource;
        $this->streamFactory = $streamFactory;
        $this->staticFilesDir = trim($staticFilesDir, '/');
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return bool
     */
    public function shouldServeFile(ServerRequestInterface $request): bool
    {
        return $this->fileSource->shouldServeFile($request)
            && is_file($this->getFilename($request));
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function serveFile(ServerRequestInterface $request): ResponseInterface
    {
        $response = $this->fileSource->serveFile($request);
        $response = $response->withBody(
            $this->streamFactory->createStreamFromFile($this->getFilename($request))
        );

        return $response->withHeader('Content-Length', filesize($this->getFilename($request)));
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return string
     */
    private function getFilename(ServerRequestInterface $request): string
    {
        return sprintf('/%s/%s', $this->staticFilesDir, trim($request->getUri()->getPath(), '/'));
    }
}
