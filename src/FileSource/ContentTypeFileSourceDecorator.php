<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\FileSource;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class ContentTypeFileSourceDecorator.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ContentTypeFileSourceDecorator implements FileSource
{
    /**
     * @var array
     */
    private static $defaultExtensionContentTypeMap = [
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpg',
        'png' => 'image/png',
        'ico' => 'image/x-icon',
        'svg' => 'image/svg+xml',
        'json' => 'application/json;charset=utf8',
        'js' => 'text/javascript;charset=utf8',
        'css' => 'text/css;charset=utf8',
    ];

    /**
     * @var FileSource
     */
    private $fileSource;
    /**
     * @var array
     */
    private $extensionContentTypeMap;

    /**
     * ContentTypeFileSourceDecorator constructor.
     *
     * @param FileSource $fileSource
     * @param array|null $extensionContentTypeMap
     */
    public function __construct(FileSource $fileSource, array $extensionContentTypeMap = null)
    {
        $this->fileSource = $fileSource;
        $this->extensionContentTypeMap = $extensionContentTypeMap ?? self::$defaultExtensionContentTypeMap;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return bool
     */
    public function shouldServeFile(ServerRequestInterface $request): bool
    {
        return $this->fileSource->shouldServeFile($request)
            && array_key_exists($this->getExtension($request), $this->extensionContentTypeMap);
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function serveFile(ServerRequestInterface $request): ResponseInterface
    {
        $response = $this->fileSource->serveFile($request);

        return $response->withHeader('Content-Type', $this->extensionContentTypeMap[$this->getExtension($request)]);
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return string
     */
    private function getExtension(ServerRequestInterface $request): string
    {
        return pathinfo($request->getUri()->getPath(), PATHINFO_EXTENSION);
    }
}
