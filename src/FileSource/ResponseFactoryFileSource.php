<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\FileSource;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class ResponseFactoryFileSource.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ResponseFactoryFileSource implements FileSource
{
    /**
     * @var ResponseFactoryInterface
     */
    private $responseFactory;

    /**
     * ResponseFactoryFileSource constructor.
     *
     * @param ResponseFactoryInterface $responseFactory
     */
    public function __construct(ResponseFactoryInterface $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public function shouldServeFile(ServerRequestInterface $request): bool
    {
        return 'GET' === $request->getMethod();
    }

    public function serveFile(ServerRequestInterface $request): ResponseInterface
    {
        return $this->responseFactory->createResponse();
    }
}
