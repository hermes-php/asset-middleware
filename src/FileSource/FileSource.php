<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\FileSource;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface FileSource.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface FileSource
{
    /**
     * Whether this FileSource should serve the requested uri based on the Request
     * information.
     *
     * @param ServerRequestInterface $request
     *
     * @return bool
     */
    public function shouldServeFile(ServerRequestInterface $request): bool;

    /**
     * Serves the file as an HTTP Response.
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function serveFile(ServerRequestInterface $request): ResponseInterface;
}
