<?php

/*
 * This file is part of the Hermes\AssetMiddleware library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Asset\Factory;

use Hermes\Asset\AssetMiddleware;
use Hermes\Asset\Exception\FactoryException;
use Hermes\Asset\FileSource\FileSource;
use Psr\Container\ContainerInterface;

/**
 * Class AssetMiddlewareFactory.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class AssetMiddlewareFactory
{
    /**
     * @var string
     */
    private $fileSourceService;

    /**
     * AssetMiddlewareFactory constructor.
     *
     * @param string $fileSourceService
     */
    public function __construct(string $fileSourceService = FileSource::class)
    {
        $this->fileSourceService = $fileSourceService;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return AssetMiddleware
     */
    public function __invoke(ContainerInterface $container)
    {
        if (!$container->has($this->fileSourceService)) {
            throw FactoryException::fromRequiredService($this->fileSourceService);
        }

        return new AssetMiddleware($container->get($this->fileSourceService));
    }
}
