# CHANGELOG

## v2.0.0 (21.01.2019)

- Simplified version 1
- Changed from Strategy pattern to Decorator.
- 100% Test Coverage